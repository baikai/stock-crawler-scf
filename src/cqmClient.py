# -*- coding: utf-8 -*-
import logging
import simplejson as json
from config import TC_CMQ_TOPIC, TC_SECRET_ID, TC_SECRET_KEY
from cmq.qcloudapi import CMQApi

logger = logging.getLogger()
logger.setLevel(logging.INFO)

config = {
    'Region': 'ap-shanghai',
    'secretId': TC_SECRET_ID,
    'secretKey': TC_SECRET_KEY
}


class CQMClient(object):
    def __init__(self):
        self.cmq_svc = CMQApi(config)

    def publish(self, data):
        action = 'PublishMessage'
        action_params = {'topicName': TC_CMQ_TOPIC, 'msgBody': json.dumps(data, use_decimal=True)}
        self.cmq_svc.call(action, action_params)


cqmClient = CQMClient()

if __name__ == '__main__':
    cqmClient.publish('Got cmq!')
