#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
from crawler import Crawler

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def main_handler(event, context):
    logger.info('start fetch stock daily data.')
    crawler = Crawler()
    crawler.craw_stock_data()
