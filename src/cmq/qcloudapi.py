#! /usr/bin/env python
# -*- coding: utf-8 -*-
from .cmq import Cmq


class CMQApi(object):
    def __init__(self, config):
        self.config = config

    def setSecretId(self, secretId):
        self.config['secretId'] = secretId

    def setSecretKey(self, secretKey):
        self.config['secretKey'] = secretKey

    def setRequestMethod(self, method):
        self.config['method'] = method

    def setRegion(self, region):
        self.config['region'] = region

    def setSignatureMethod(self, SignatureMethod):
        self.config['SignatureMethod'] = SignatureMethod

    def generateUrl(self, action, params):
        service = Cmq(self.config)
        return service.generateUrl(action, params)

    def call(self, action, params, req_timeout=None, debug=False):
        """
        @type action: string
        @param action: action interface

        @type params: dict
        @param params: interface parameters

        @type req_timeout: int
        @param req_timeout: request timeout(seconds)

        @type debug: bool
        @param debug: debug switch
        """
        service = Cmq(self.config)
        if req_timeout is not None:
            service.set_req_timeout(req_timeout)
        if debug:
            service.open_debug()

        methods = dir(service)
        for method in methods:
            if (method == action):
                func = getattr(service, action)
                return func(params)

        return service.call(action, params)
