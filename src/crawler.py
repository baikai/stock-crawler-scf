# -*- coding: utf-8 -*-
import logging
import os
import json
import asyncio
import aiohttp
import traceback
from cqmClient import cqmClient
from wstockQuery import WStockQuery

DAILY = 0
WEEKLY = 2
MONTHLY = 1
DAILY_COROU_NUMBER = 150

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class Crawler(object):
    def __init__(self):
        self.stock_list = self._load_stock_list_file()
        self.cqm_client = cqmClient
        self.stock_query = WStockQuery()

    def craw_stock_data(self):
        loop = asyncio.get_event_loop()
        try:
            loop.run_until_complete(self.get_stock_daily_data(loop))
        except Exception as e:
            traceback.print_exc()
            logging.error('daily fetching error {} '.format(e))
        finally:
            loop.close()

    async def get_stock_daily_data(self, loop):
        stock_slices = self.split_stock_list(self.stock_list, DAILY_COROU_NUMBER)
        async with aiohttp.ClientSession(loop=loop) as session:
            tasks = [asyncio.ensure_future(self.daily_job(session, each_slice)) for each_slice in stock_slices]
            responses = asyncio.gather(*tasks)
            await responses

    async def daily_job(self, session, stocks):
        for stock in stocks:
            market, stock_id = self._split_code_string(stock)
            finance_data = await self.stock_query.get_stock_finance_data(session, market, stock_id)
            for category in [DAILY, WEEKLY, MONTHLY]:
                if category == DAILY:
                    message_type = 'dayBar'
                elif category == WEEKLY:
                    message_type = 'weekBar'
                else:
                    message_type = 'monthBar'
                k_data = await self.stock_query.get_k_data(session, category, market, stock_id)
                stock_dict = dict()
                stock_dict.update({'stock_id': stock_id})
                stock_dict.update({'messageType': message_type})
                stock_dict.update({'stockData': k_data})
                stock_dict.update({'finance': finance_data})
                self.cqm_client.publish(stock_dict)

    @staticmethod
    def split_stock_list(stock_list, slice_num):
        if len(stock_list) < slice_num:
            size = 1
        else:
            size = round(len(stock_list) / slice_num)
        return [stock_list[i:i + size] for i in range(0, len(stock_list), size)]

    @staticmethod
    def _load_stock_list_file():
        folder_path = os.path.dirname(__file__)
        stock_list = json.load(open(os.path.join(folder_path, 'stockList.json')))['stockList']
        return list(map(lambda x: x['id'], stock_list))

    @staticmethod
    def _split_code_string(stock_id):
        return stock_id[0], stock_id[1: len(stock_id)]


if __name__ == '__main__':
    crawler = Crawler()
    crawler.craw_stock_data()
