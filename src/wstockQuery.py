#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
import os
from decimal import Decimal
import async_timeout
from config import K_API, FINANACE_API

logger = logging.getLogger()
logger.setLevel(logging.INFO)


class WStockQuery(object):
    def __init__(self):
        self.ws_username = os.environ['WSTOCK_USERNAME']
        self.ws_pwd = os.environ['WSTOCK_PASSWORD']

    async def get_k_data(self, session, category, market, stock_id):
        market = self._get_market_id(market)
        request_url = '{k_api}?symbol={market}{stock_id}&u={username}&p={password}&r_type=2&return_t={category}&' \
                      'fq=1&num=1'.format(k_api=K_API, market=str(market), stock_id=str(stock_id),
                                      username=self.ws_username, password=self.ws_pwd, category=str(category))
        logging.info("Request to wstock kline api: " + request_url)
        k_data = await self._fetch_wstock_http_url(session, request_url)
        if self.verify_response(k_data) is True:
            k_data = self._transform_k_data(k_data)
        return k_data

    async def get_stock_finance_data(self, session, market, stock_id):
        market = self._get_market_id(market)
        request_url = '{finance_api}?symbol={market}{stock_id}&u={username}&p={password}&r_type=2'.format(
            finance_api=FINANACE_API, market=str(market), stock_id=str(stock_id), username=self.ws_username, 
            password=self.ws_pwd)
        logging.info("Request to wstock finance api: " + request_url)
        finance_data = await self._fetch_wstock_http_url(session, request_url)
        if self.verify_response(finance_data[0]) is True:
            finance_data = self._transform_finance_data(finance_data[0])
        return finance_data

    @staticmethod
    def _transform_k_data(data):
        return list(map(lambda item: {
            'open': Decimal(item['Open']),
            'close': Decimal(item['Close']),
            'high': Decimal(item['High']),
            'low': Decimal(item['Low']),
            'volume': Decimal(item['Volume']),
            'amount': Decimal(item['Amount']),
            'datetime': item['Date']
        }, data))

    @staticmethod
    def _transform_finance_data(data):
        new_data = dict()
        new_data['DM_01'] = data['DM_01']
        new_data['BGQ_02'] = data['BGQ_02']
        new_data['SSRQ_03'] = data['SSRQ_03']
        new_data['MGSY_04'] = Decimal(data['MGSY_04'])
        new_data['MGJZC_05'] = Decimal(data['MGJZC_05'])
        new_data['JZCSYL_06'] = Decimal(data['JZCSYL_06'])
        new_data['MGJYXJ_07'] = Decimal(data['MGJYXJ_07'])
        new_data['MGGJJ_08'] = Decimal(data['MGGJJ_08'])
        new_data['MGWFP_09'] = Decimal(data['MGWFP_09'])
        new_data['GDQYB_10'] = Decimal(data['GDQYB_10'])
        new_data['JLRTB_11'] = Decimal(data['JLRTB_11'])
        new_data['ZYSRTB_12'] = Decimal(data['ZYSRTB_12'])
        new_data['XSMLL_13'] = Decimal(data['XSMLL_13'])
        new_data['TZMGJZC_14'] = Decimal(data['TZMGJZC_14'])
        new_data['ZZC_15'] = Decimal(data['ZZC_15'])
        new_data['LDZC_16'] = Decimal(data['LDZC_16'])
        new_data['GDZC_17'] = Decimal(data['GDZC_17'])
        new_data['WXZC_18'] = Decimal(data['WXZC_18'])
        new_data['LDFZ_19'] = Decimal(data['LDFZ_19'])
        new_data['CQFZ_20'] = Decimal(data['CQFZ_20'])
        new_data['ZFZ_21'] = Decimal(data['ZFZ_21'])
        new_data['GDQY_22'] = Decimal(data['GDQY_22'])
        new_data['ZBGJJ_23'] = Decimal(data['ZBGJJ_23'])
        new_data['JYXJLL_24'] = Decimal(data['JYXJLL_24'])
        new_data['TZXJLL_25'] = Decimal(data['TZXJLL_25'])
        new_data['CZXJLL_26'] = Decimal(data['CZXJLL_26'])
        new_data['XJZJE_27'] = Decimal(data['XJZJE_27'])
        new_data['ZYSR_28'] = Decimal(data['ZYSR_28'])
        new_data['ZYLR_29'] = Decimal(data['ZYLR_29'])
        new_data['YYLR_30'] = Decimal(data['YYLR_30'])
        new_data['TZSY_31'] = Decimal(data['TZSY_31'])
        new_data['YYWSZ_32'] = Decimal(data['YYWSZ_32'])
        new_data['LRZE_33'] = Decimal(data['LRZE_33'])
        new_data['JLR_34'] = Decimal(data['JLR_34'])
        new_data['WFPLR_35'] = Decimal(data['WFPLR_35'])
        new_data['ZGB_36'] = Decimal(data['ZGB_36'])
        new_data['WXSGHJ_37'] = Decimal(data['WXSGHJ_37'])
        new_data['AG_38'] = Decimal(data['AG_38'])
        new_data['BG_39'] = Decimal(data['BG_39'])
        new_data['JWSSG_40'] = Decimal(data['JWSSG_40'])
        new_data['QTLTG_41'] = Decimal(data['QTLTG_41'])
        new_data['XSGHJ_42'] = Decimal(data['XSGHJ_42'])
        new_data['GJCG_43'] = Decimal(data['GJCG_43'])
        new_data['GYFRG_44'] = Decimal(data['GYFRG_44'])
        new_data['JNFRG_45'] = Decimal(data['JNFRG_45'])
        new_data['JNZRRG_46'] = Decimal(data['JNZRRG_46'])
        new_data['QTFQRG_47'] = Decimal(data['QTFQRG_47'])
        new_data['MJFRG_48'] = Decimal(data['MJFRG_48'])
        new_data['JWFRG_49'] = Decimal(data['JWFRG_49'])
        new_data['JWZRRG_50'] = Decimal(data['JWZRRG_50'])
        new_data['YXGHQT_51'] = Decimal(data['YXGHQT_51'])
        return new_data

    @staticmethod
    async def _fetch_wstock_http_url(session, url):
        with async_timeout.timeout(120):
            async with session.get(url) as responses:
                return await responses.json()

    @staticmethod
    def verify_response(response):
        for k in response:
            if k == 'errcode' or k == 'errmsg':
                return False
        return True

    @staticmethod
    def _get_market_id(market):
        if market is '0' or market is 0:
            return 'SZ'
        elif market is '1' or market is 1:
            return 'SH'
        else:
            return market
