FROM python:3.6.3

RUN apt-get update && apt-get -y install zip unzip

RUN mkdir app
RUN mkdir /artifacts
VOLUME /artifacts

WORKDIR app

COPY src/cmq ./cmq
COPY src/*.py ./
COPY src/stockList.json .
COPY requirements.txt .

RUN pip install -r requirements.txt -t .

RUN zip -r stock_daily_crawler.zip *

RUN chmod +rx stock_daily_crawler.zip