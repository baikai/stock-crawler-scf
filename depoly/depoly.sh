#!/usr/bin/env bash


DOCKER_REGISTRY=ccr.ccs.tencentyun.com/stockapp
APP_IMAGE_LATEST="stock-daily-crawler-builder"


# build the artifacts
docker build -t ${APP_IMAGE_LATEST} .
docker run --rm -i -v ${PWD}/artifacts:/artifacts  stock-daily-crawler-builder cp stock_daily_crawler.zip /artifacts